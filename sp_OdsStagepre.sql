/****** Cannot script Unresolved Entities : Server[@Name='wmcbiapp']/Database[@Name='biData']/UnresolvedEntity[@Name='Particiones' and @Schema='mop'] ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [mon].[Atributo]
(
	[idSist] [int] NOT NULL,
	[idBd] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[idSch] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[idEntidad] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[idAtrib] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[nuOrdAtrib] [int] NULL,
	[idAtribCli] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[idDato] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ltDescAtrib] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ltFmt] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ltTipoDato] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[nuChar] [smallint] NULL,
	[nuPrec] [smallint] NULL,
	[nuEscala] [smallint] NULL,
	[inPk] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[nuOrdPk] [int] NULL,
	[inUmbral] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ltValInf] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ltValSup] [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inDom] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inLimInf] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inLimSup] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inNulo] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inMapAtr] [smallint] NULL,
	[cdMapAtr] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ltMapAtr] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inValFec] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,

 CONSTRAINT [PK_Atributo]  PRIMARY KEY NONCLUSTERED 
(
	[idSist] ASC,
	[idBd] ASC,
	[idSch] ASC,
	[idEntidad] ASC,
	[idAtrib] ASC
)
)WITH ( MEMORY_OPTIMIZED = ON , DURABILITY = SCHEMA_AND_DATA )

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [mop].[sp_OdsStagepre]
	@fcInf int,
	@idSist int,
	@idBd [nvarchar](25),
	@idSch nvarchar(3),
	@idEntidad nvarchar(50)
AS
	BEGIN
	DECLARE 
		@idAtrib nvarchar(50),
		@ltFmt nvarchar(50),
		@inNulo nvarchar(1),
		@ltNull nvarchar(25),
		@sql nvarchar(max),
		@nuParticion int,
		@ltTableSpace nvarchar(100),
		@ltPathTabla nvarchar(100),
		@inPrimerAtr int

	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	/**
	--- Construye el nombre de la tabla
	--- Verifica si existe.
	--- Si existe la borra
	**/
	BEGIN TRY
	SET @ltPathTabla = 	@idBd +'.pre.'+ @idEntidad ;
		SET @sql = 'DROP TABLE ' + @ltPathTabla + ' ;';
		IF OBJECT_ID(@ltPathTabla, 'U') IS NOT NULL 
	BEGIN
		EXEC sp_executesql
		@stmt = @sql;
	END

	/**
	--- Obtiene el número de partición y el tablespace
	**/

	SELECT  @nuParticion = nuParticion,
			@ltTableSpace = ltTableSpace
	FROM [biData].[mop].[Particiones]
	WHERE	fcInf = @fcInf
	AND		idSist = @idSist
	AND		idBd = @idBd
	AND		idSch = 'ods'
	AND		idEntidad = 'Todas';

	/**
	-- Generar Create Table
	**/
	SET @inPrimerAtr = 0;
	SET @sql = 'CREATE TABLE ' + @idBd + '.pre.' +  @idEntidad + ' ( '

	DECLARE C CURSOR FAST_FORWARD  FOR
	
	SELECT idAtrib, ltFmt, inNulo
	FROM [biData].[mon].[Atributo]
	WHERE	idSist = @idSist
	  AND	idBd = @idBd
	  AND	idSch = @idSch
	  AND	idEntidad = @idEntidad	
	ORDER BY nuOrdAtrib;

	OPEN C;

	FETCH NEXT FROM C INTO @idAtrib, @ltFmt, @inNulo;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @inPrimerAtr = 1
			SET @sql = @sql + ', '
		ELSE
			SET @inPrimerAtr = 1;
		IF @inNulo = 'S' 
			SET @ltNull = 'NULL';
		ELSE
			SET @ltNull = 'NOT NULL';
		SET @sql = @sql + @idAtrib + ' ' + @ltFmt + ' ' + @ltNull;

		FETCH NEXT FROM C INTO @idAtrib, @ltFmt, @inNulo;
	END

	SET @sql = @sql + ' ) ON [' + @ltTableSpace + ']';
	CLOSE C;


		EXEC sp_executesql
		@stmt = @sql;

		DEALLOCATE C;
	
	/**
	-- Crear el índice de columnas
	**/

	SET @sql = N'CREATE CLUSTERED COLUMNSTORE INDEX ClustStoIdx_' + @idEntidad + N' ON ' + @ltPathTabla + N' WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [' + @ltTableSpace + ']';

		EXEC sp_executesql
		@stmt = @sql;
		SELECT @@ERROR AS [error], ERROR_MESSAGE() AS [error_msg];


	/**
	-- Crear constrain de la particion
	**/

	SET @sql = 'ALTER TABLE ' + @idBd +'.pre.'+ @idEntidad  + ' WITH CHECK ADD  CONSTRAINT chk_' + @idEntidad + ' CHECK  ([fcInf] = ' + CAST(@fcInf as nvarchar(6)) +  ')';

	EXEC sp_executesql
		@stmt = @sql;

	END TRY

	BEGIN CATCH
		SELECT @@ERROR AS [error], ERROR_MESSAGE() AS [error_msg];
		SELECT @sql;
		
	END CATCH
	
	

END
