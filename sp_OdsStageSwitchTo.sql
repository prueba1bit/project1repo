/****** Cannot script Unresolved Entities : Server[@Name='wmcbiapp']/Database[@Name='biData']/UnresolvedEntity[@Name='Particiones' and @Schema='mop'] ******/

-- Batch submitted through debugger: SQLQuery8.sql|7|0|C:\Users\usrBioDb_bio\AppData\Local\Temp\5\~vs98CB.sql
CREATE PROCEDURE [mop].[sp_OdsStageSwitchTo]
	@fcInf int,
	@idSist int,
	@idBd [nvarchar](25),
	@idEntidad nvarchar(50)

AS
	BEGIN
	DECLARE 
		@sql nvarchar(4000),
		@nuParticion int,
		@cdResult int,
		@idSch nvarchar(3),
		@ltErr nvarchar(4000)


	SET NOCOUNT ON;
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON
	SET @cdResult = 0;
	
	
	/**
	--- Obtiene el número de partición y el tablespace
	**/

	BEGIN TRY
	SELECT  @nuParticion = nuParticion
	FROM [biData].[mop].[Particiones]
	WHERE	fcInf = @fcInf
	AND		idSist = @idSist
	AND		idBd = @idBd
	AND		idSch = 'ods'
	AND		idEntidad = 'Todas';

	/**
	-- activar constrains de particion
	**/

	SET @sql = 'ALTER TABLE ' + @idBd +'.pre.'+ @idEntidad  + ' CHECK CONSTRAINT chk_' + @idEntidad;
	EXEC sp_executesql
		@stmt = @sql;

	/**
	-- Generar Swith-In Table Partition
	**/
	IF @idEntidad = 'errDqs'
		SET @idSch = 'err'
	ELSE
		SET @idSch = 'ods';

	SET @sql = 'ALTER TABLE ' + @idBd + '.pre.' + @idEntidad + ' SWITCH TO ' +  @idBd + '.' + @idSch + '.' + @idEntidad  + ' PARTITION ' + CAST(@nuParticion AS nvarchar(4)) + ' WITH (WAIT_AT_LOW_PRIORITY (MAX_DURATION = 0 MINUTES, ABORT_AFTER_WAIT = NONE));'

	
		EXEC sp_executesql
			@stmt = @sql;	

		SELECT @cdResult = @@ERROR;
	
		IF (@cdResult = 0) 
		BEGIN
			SET @sql = 'DROP TABLE ' + @idBd + '.pre.' + @idEntidad ;
			EXEC sp_executesql
			@stmt = @sql;
		END;
		SELECT @@ERROR AS [error], ERROR_MESSAGE() AS [error_msg];
	END TRY

	BEGIN CATCH
		SELECT @@ERROR AS [error], ERROR_MESSAGE() AS [error_msg];

	END CATCH
		
END
